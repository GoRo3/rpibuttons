import QtQuick 2.0
import QtQuick.Controls.Material 2.3
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "./"

Rectangle {
    id: rectangle
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    width: visible ? 150 : 0
    x: visible ? parent.width - width : parent.width
    visible: false

    Connections {
        target: soundLogic
        onSetSliderValue: {
            microSlider.value = a_value
        }
    }

    Slider {
        id: microSlider
        x: 51
        y: 108
        width: 48
        height: 264
        smooth: true
        antialiasing: true
        snapMode: Slider.SnapOnRelease
        stepSize: 5
        to: 255
        hoverEnabled: true
        wheelEnabled: true
        anchors.verticalCenterOffset: -28
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        orientation: Qt.Vertical
        value: 0

        ToolTip.delay: 1000
        ToolTip.timeout: 5000
        ToolTip.visible: hovered
        ToolTip.text: qsTr("Regulacja czułości mikrofonu")

        ToolTip {
                parent: microSlider.handle
                visible: microSlider.pressed
                text: microSlider.value.toFixed(1)
            }

        onValueChanged: {
            soundLogic.sliderValueChenge(microSlider.value);
        }
    }

    Text {
        id: element
        x: 40
        y: 27
        width: 70
        height: 25
        text: qsTr("Głośność")
        anchors.horizontalCenterOffset: 1
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 17
    }

    RadioButton {
        id: muteButton
        x: 41
        y: 389
        text: "Mute"
        wheelEnabled: false
        autoExclusive: false
        display: AbstractButton.TextBesideIcon
        autoRepeat: false
        checked: false
        onCheckedChanged: {
            soundLogic.muteButtonPressed(muteButton.checked);
            microSlider.enabled = muteButton.checked ? false : true;
        }
    }



}


