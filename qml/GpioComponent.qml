import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.VirtualKeyboard 2.1
import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.VirtualKeyboard 2.4

/*
  Dostarczony model w systemie Model/View odpowiada za przekazanie informacji
  o tym co się dzieje w warstwie logicznej do warstwy prezentacji. Do zaprezentowania
  w GUI pojedyńczych elementów naszego modelu służy komponent "delegate", a poniżej przygotowaliśmy
  własną nie standardową jego wersję. Poza własną konstrukcją takich obiektów można skorzystać z gotówców
  dostarczonych przez Qt'a.

  Linki:
   - https://doc.qt.io/qt-5/qml-qtquick-controls2-itemdelegate.html
   - https://doc-snapshots.qt.io/qt5-5.11/qtquick-modelviewsdata-modelview.html
*/
import QtQuick.Templates 2.5

Button {
    id: button
    property alias itemName: itemName
    property alias labelPin: labelPin

    antialiasing: true

    background: Rectangle {

        id: test
        radius: 15

        color: model.state ? Material.color(Material.Red) : Material.color(Material.Grey)

        Label {
                id: labelPin
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenterOffset: 25
                text: model.pin
                font.bold: true
                font.pixelSize: 20
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            TextInput {
                id: itemName
                activeFocusOnPress: false
                anchors.top: parent.top
                anchors.topMargin: 15
                width: (parent.width*0.8)
                height: 20
                anchors.horizontalCenter: parent.horizontalCenter
                font.family: "Arial"
                horizontalAlignment: Text.AlignHCenter
                font.weight: Font.Light
                font.pixelSize: 24
                maximumLength: 25

                EnterKeyAction.actionId: EnterKeyAction.Done
                EnterKeyAction.label:"Done"
                }
    }
}


