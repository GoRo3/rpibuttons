import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.0
import QtQuick.VirtualKeyboard 2.4
import QtQuick.Window 2.0

import "./"

/*
  Tutaj zaczyna się główne okno programu. Plik swoją rolą przypomina
  rolę pliku main.cpp. Dzięki niemu zostaje zbudowany orez wyświelone okno
  oraz wszystkie komponenty.

  Jako, że cała aplikacja opiera się na paradygmacie model/view, wszystkie
  komponenty QML opowiadają ze prezentację aplikacji. Za pomocą sygnałów oraz slotów
  warstwa prezentacji komunikuje się z warstwą logiczną napisaną w C++.

  QML jest to wymyślony na potrzeby pisania aplikacji w Qt język definiujący wygląd
  pojedyńczych komponentów. Najwazniejsze jego aspekty to fakt, że wszystkie komponenty
  są napisane w C++ dlatego program działa sprawnie i responsywnie. Na uwagę zasługuje
  również fakt, że QML doskonale integruje się z Językiem JavaScript, i można deklarować
  funkcje oraz wstawki korzystające z JS. Szczegółowe informację znajdują się w
  załączonym linku.

  W przypadku większych aplikacji dobrą praktyką jest oddzielanie warstwy prezentacji
  od warstwy logicznej nawet na poziomie wątków. Tworzyt się nowy wątek, w którym pracuje
  warstwa logiczna i przesyła tylko sygnały do warstwy aplikacji. Ponieważ program jest
  w miarę prosty i zmiana stanu gpio nie powoduje niskiej responsywności GUI, nie
  budowałem takiego podziału.

  Poniżej przedstawione zostały linki do dokumentacji Qt:
   - https://doc.qt.io/qt-5/model-view-programming.html
   - https://doc.qt.io/qt-5/qtqml-index.html
  */

ApplicationWindow {
    id: appWindow
    visible: true
    title: qsTr("GPIO CONTROL")

    width:  Screen.width
    height: Screen.height
    visibility: Window.FullScreen

    Material.theme: Material.Light

    property alias appConteiner: appConteiner
    property alias exitPopup: exitPopup
    property alias shutdownPopup: shutdownPopup

    Rectangle {
        id: appConteiner
        anchors.left: optionsMenu.right
        anchors.top: parent.top
        anchors.right: soundMenu.left
        anchors.bottom: inputPanel.top

        Rectangle {
            id: options
            y: 0
            width: 50
            height: 50
            anchors.verticalCenterOffset: 5
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 5
            MouseArea{
                anchors.fill: parent
                Image {
                    anchors.fill: parent
                    width: options.width
                    height: options.height

                    id: optionsIcon
                    source: optionsMenu.visible ? "qrc:/double-left.png" : "qrc:/double-right.png"
                }
                antialiasing: true
                onClicked: {
                    optionsMenu.visible = optionsMenu.visible ? false : true
                }
            }
        }

        Rectangle {
            id: sound
            y: 0
            width: 50
            height: 50
            anchors.verticalCenterOffset: 5
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.leftMargin: 5
            MouseArea{
                anchors.fill: parent
                Image {
                    anchors.fill: parent
                    width: sound.width
                    height: sound.height

                    id: soundIcon
                    source: soundMenu.visible ? "qrc:/double-right.png" : "qrc:/double-left.png"
                }
                antialiasing: true
                onClicked: {
                    soundMenu.visible = soundMenu.visible ? false : true
                }
            }
        }
        /*
        GridView jest to komponent odpowiedzialny za wyświetlenie w określonym stylu
        (w tym przypadu jest to kratka), elemetnów modelu napisanego w C++.
        */
        GridView {
            id: gridLayout

            anchors.rightMargin: 50
            anchors.leftMargin: 60
            anchors.bottomMargin: 50
            anchors.topMargin: 50
            anchors.fill: parent

            cellHeight: (height/3)
            cellWidth: (width/5)

            model: gpioModel
            delegate: GpioComponent {
                height: (gridLayout.cellHeight-10)
                width: (gridLayout.cellWidth-10)
                itemName.text: model.name

                onClicked: {
                    gpioModel.toglePin(index);
                }
                itemName.onAccepted: {
                    model.name = itemName.text
                    itemName.deselect()
                    itemName.activeFocusOnPress = false;
                    itemName.cursorVisible = false;
                    labelPin.visible = true;
                }
                onPressAndHold: {
                    itemName.activeFocusOnPress = true;
                    itemName.forceActiveFocus()
                    itemName.selectAll()
                    labelPin.visible = false;
                }
            }
        }
    }

    OptionsMenu {
        id: optionsMenu

        save.onClicked: {
            gridLayout.model.saveModel() ? savePopup.open() : console.log("Nie udało się zapisać")
        }
        load.onClicked: {
            gridLayout.model.loadModel() ? loadPopup.open() : console.log("Nie udało się wczytać pliku")
        }
    }

    SoundMenu {
        id: soundMenu
    }

    Popup {
        id: savePopup
        padding: 10
        anchors.centerIn: parent
        width: 200
        height: 100
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        exit: Transition {
                NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }

        ColumnLayout {
            anchors.fill: parent
            spacing: 2
        Text {
            id: description
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Zapisano poprawnie")
        }

        Button {
            id: ok
            text: "OK"
            Layout.alignment: Qt.AlignCenter
            onClicked: {
                savePopup.close()
            }
          }
        }
    }

    Popup {
        id: loadPopup
        padding: 10
        anchors.centerIn: parent
        width: 200
        height: 100
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        exit: Transition {
                NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }

        ColumnLayout {
            anchors.fill: parent
            spacing: 2

        Text {
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Wczytano poprawnie")
        }

        Button {
            text: "OK"
            Layout.alignment: Qt.AlignCenter
            onClicked: {
                loadPopup.close()
            }
          }
        }
    }

    Popup {
        id: shutdownPopup
        padding: 10
        anchors.centerIn: parent
        width: infoText.width + 10
        height: 100
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        exit: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }

        ColumnLayout {
            anchors.fill: parent
            spacing: 2
            Text {
                id: infoText
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Czy na pewno chcesz wyłączyć system?")
            }
            Row {
                spacing: 15
                Layout.alignment: Qt.AlignCenter
                Button {
                    id: tak
                    text: "TAK"
                    onClicked: {
                        utils.shutdownSystem()
                    }
                }
                Button {
                    id: nie
                    text: "NIE"
                    onClicked: {
                        shutdownPopup.close()
                    }
                }
            }
        }
    }

    Popup {
        id: exitPopup
        padding: 10
        anchors.centerIn: parent
        width: exitInfo.width+10
        height: 100
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        exit: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }

        ColumnLayout {
            anchors.fill: parent
            spacing: 2
            Text {
                id: exitInfo
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Czy na pewno chcesz wyjść?")
            }
            Row {
                spacing: 15
                Layout.alignment: Qt.AlignCenter
                Button {
                    text: "TAK"
                    onClicked: {
                        Qt.quit()
                    }
                }
                Button {
                    text: "NIE"
                    onClicked: {
                        exitPopup.close()
                    }
                }
            }
        }
    }
    /*
      InputPanel służy jako część virtualnej klawiatury i odpowiada ze jej wyświetlenie.
   */
    InputPanel {
        id: inputPanel
        y: Qt.inputMethod.visible ? parent.height - inputPanel.height : parent.height
        anchors.left: parent.left
        anchors.right: parent.right
    }
}
















