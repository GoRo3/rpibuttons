import QtQuick 2.0
import QtQuick.Controls.Material 2.3
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "./"
/*
  Przycisk odpowiedzialny za wyjście z programu. Ze względu na sterowniki jakie są
  wykorzystane do wyświetlania programów przez Raspberiana, okno programu jest
  zawsze na wierzchu i aby je zamknąć należy zaimplementować odpowiedni przycisk.
*/

Rectangle {
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    width: visible ? 150 : 0
    x: visible ? parent.width - width : parent.width
    visible: false

    property alias save: columnLayout.saveButton
    property alias load: columnLayout.loadButton

    Column {
        id: columnLayout
        spacing: 5

        property alias saveButton: saveButton
        property alias loadButton: loadButton

       Button {
            width: 140
            height: 80
            text: "Shutdown"
            antialiasing: true
            onClicked: {
                shutdownPopup.open()
            }
        }

        Button {
            width: 140
            height: 80
            text: "QUIT"
            antialiasing: true
            onClicked: {
                exitPopup.open()
            }
        }

        ToolSeparator{
            orientation: Qt.Horizontal
            width: parent.width
        }

        Button {
            id: saveButton
            width: 140
            height: 80
            text: "SAVE"
            antialiasing: true
        }

        Button {
            id: loadButton
            width: 140
            height: 80
            text: "LOAD"
            antialiasing: true
        }
    }
}

