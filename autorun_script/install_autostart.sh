#!/bin/bash

sudo chmod 644 buttons.service
sudo cp buttons.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable buttons.service

echo "**********************************************************************"
echo "* 		Konfiguracja zakończona pomyślnie!	"  
echo "* Aby wszystko działało prawidłowo umieść program w katalogu: "
echo "*		 '/home/pi/Rpi3_Buttons/Rpi3_Buttons'  		           "
echo "**********************************************************************"
exit 0
