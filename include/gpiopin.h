﻿#ifndef GPIOPIN_H
#define GPIOPIN_H

#include <QObject>
#include <QDataStream>

/*
 * Klasa odpowiada za przedstawienie w programie relacji pinu Gpio. Tworząc instanjcę klasy
 * otwarzymy połączenie pomiędzy programem a jednym pinem Gpio.
 *
 * Aby sterować pinami zostały przygotowane metody nazywane - setters - oraz aby sprawdzić stany
 * logiczne - getters -.
 */
class GpioPin
{
    Q_GADGET

    Q_PROPERTY(QString name READ pinName WRITE setPinName)

public:
    /*
     * Enum przeznaczony jest do reprezentacji stanu pinu.
     *
     * eLow Stan niski
     * eHigh Stan wysoki
     */
    enum class State {
        eLow = 0,
        eHigh,
    };

    /*
     *
     * Enum reprezentujący za kierunek pinu.
     *
     *  eIn Pin ustawiony jako wejście
     *  eOut Pin ustawiony jako wyście
     */
    enum class Direction {
        eIn = 0,
        eOut,
    };

public:
    /*
     *  GpioPin::GpioPin();
     *  GpioPin::GpioPin(int a_pin, Direction m_pinDirection = Direction::eOut);
     *
     *  Konstruktory klasy GPIO, w tym kontruktor domyslny oraz konstruktor przyjmujący parametry
     *  "a_pin" oraz "m_pinDirection", które odpowiadają za numer pinu w Raspberry oraz czy będzie traktowany jako
     *  wyjście czy jako wejście.
     *
     */
    GpioPin();
    GpioPin(int a_pin, Direction m_pinDirection = Direction::eOut);

    /*
     * Metoda (getter) pinNumber zwaraca pin pod który podpięta jest instancja klasy
     */
    int pinNumber() const;

    /*
     * Matoda (setter) setPinNumber ustawia numer pinu do obsługi. Funkcja jest przydatna jeśli stworzyliśmy obiekt klasy
     * przy pomocy kontruktora domyślnego, wtedy należy ustawić odpowiedni pin.
     */
    void setPinNumber(int pinNumber);

    /*
     *  Funkcja (getter) zwracająca aktualny stan pinu. Zwracana wartośc jest reprezentowana przez enum State
     */
    State pinState() const;
    /*
     *  Funkcja (setter) przeznaczona do ustawienia odpowiedniego stanu pinu. Przyjmuje argument enum State.
     */
    void setPinState(State pinState);

    /*
     * Funkcja (getter) zwracająca aktualny kierunkek pinu. Pokazuje nam czy pin jest ustawiony jako wyjście czy wejście.
     * Zwraca enum Direction.
     */
    Direction pinDirection() const;

    /*
     * Funkcja (setter) ustawian kierunek pinu. Czy ma być wyjściem czy wejściem. Jako argument przyjmuje Enum Direction
     */
    void setPinDirection(Direction pinDirection);

    /*
     * Funkcja zmienia stan pinu w zależności od tego jaki ustwiony jest obecny stan.
     */
    void toglePin();

    QString pinName() const;
    void setPinName(const QString &pinName);

private:
    /*
     * Zmienna reprezentująca numer pinu.
     */
    int m_pinNumber{};
    /*
     * Zmienna reprezentująca stan pinu.
     */
    mutable State m_pinState{};
    /*
     * Zmienna reprezentująca kierunek pinu.
     */
    mutable Direction m_pinDirection{};

    QString m_pinName{};
};


/*
 * Przeciążone operatory << oraz >> aby móc w odpowiedni sposób serializować klasę.
 */
QDataStream &operator<<(QDataStream &out, const GpioPin &pin);
QDataStream &operator>>(QDataStream &in, GpioPin &pin);


#endif // GPIOPIN_H
