#pragma once
#include <QAbstractListModel>

#include "gpiopin.h"
#include "serializer.h"

/*
 *  Jak już wcześniej zostało wspomniane cała aplikacja opiera się na paradygmacie
 *  Model/view. Poniżej znajduje się klasa odpowiadająca za model.
 *
 *  Model jest to abstrakcyjna klasa przeznaczona do łaczenia zdarzeń z warstwy aplikacji
 *  ze obiektami stworzonymi w warstwie ligocznej. W naszym przypadku dzięki takiej architekturze
 *  po nacisnięciu przycisku na wyświetlaczu dotykowym, model dostaje informację, że obiekt pod danym
 *  indexem należy zmodyfikować - poprzez wywołanie funkcji toglePin().
 *  Drugim atutem zastosowania modelu jest fakt, że jeśli zmodyfikujemy jakiś element z poziomu warstwy
 *  logicznej to model zadba o poinformowanie warstwy aplikacji o tym, że modyfikacja została dokonana
 *  i należy odświeżyć widok.
 *
 *  Jest to podejście szeroko stosowane w budowaniu aplikacji opartych o GUI oraz innych jezykach programowania
 *  takich jak Java lub C#.
 *
 * Linki:
 *  - https://doc.qt.io/qt-5/model-view-programming.html
 *  - https://doc.qt.io/qt-5/modelview.html
 *  - https://www.cleanqt.io/blog/crash-course-in-qt-for-c%2B%2B-developers,-part-5
 */

/*
 * Makto dzięki któremy możemy przekaząć instancję klasy do warstwy plikacji.
 */
Q_DECLARE_METATYPE(QModelIndex)

class GpioPinModel : public QAbstractListModel
{
    Q_OBJECT
    /*
     * Makro określające klasę GpioModel jako nie kopiowalną.
     */
    Q_DISABLE_COPY(GpioPinModel)
    Q_ENUMS(role_names)

public:
    /*
     * Enum niezbędny do komunikacji warstwy logicznej w warstwą prezentacji.
     * Mapuje on pola klasy GpioPin do wartości zrozumiałych dla aplikacji.
     */
    enum roleNames {
      PIN_E = Qt::UserRole +1,
      STATE_E,
      DIRECTION_E,
      NAME_E
    };

    /*
     * Konstuktor naszego modelu.
     */
    explicit GpioPinModel(QObject *parent = nullptr);

    // QAbstractItemModel interface
public:
    /*
     *  Zestaw funkcji przeznaczonych do komunikacji oraz informowania
     *  warstwy aplikacji o zmianach w modelu.
     *  Jednoczesnie warswa Aplikacji może dzięki wykorzystaniu tych funkcji może modyfikować model
     *
     * Poniżeszze funkcje to niezbędne minimum aby komunikacja miała miejsce i zostały one zadeklarowane
     * w klasie QAbstractListModel, a dzieki wykorzystywaniu mechanizmu dziedziczenia, możemy je redefiniować
     * i wykorzystać na nasz użytek. W taki sposób tworzy się własne modele w Qt.
     *
     * linki: https://doc.qt.io/qt-5/qabstractlistmodel.html
     */
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    /*
     * Funkcja przeznaczona do dodawania nowego elementu GpioPin do modelu.
     */
    void addItem(GpioPin *a_gpioPin);

    /*
     * Funkcja, którą można wywołać z warsty aplikacji. W tym przypadku przyjmuje ona index elementu,
     * w którym chcemy zmienić stan pinu.
     *
     * Link: https://doc.qt.io/qt-5/qtqml-cppintegration-definetypes.html
     */
    Q_INVOKABLE void toglePin(int index);
    Q_INVOKABLE bool saveModel();
    Q_INVOKABLE bool loadModel();

    void setGpioContainer(const QVector<GpioPin *> gpioContainer);
    void setSerializer(Serializer *serializer);

signals:
    /*
     * Sygnał, który będzie emitowany za każdym razem kiedy zmieni się stan jakiegoś elementu w m_gioContainer.
     * Dzięki temu warstwa prezentacji aplikacji będzie wiedziała, że należy odświeżyć dany element.
     */
    void dataItemUpdated(int index);

private:
    /*
     * Zmienna przechowująca listę pinów w postaci wskaźnika na wczesniej utworzone obiektu (w Main.cpp)
     *  QVector reprezentuje tablicę obiektów z możliwością dynamicznie zwiększania wielkości tablicy.
     * link:
     *  - https://doc.qt.io/qt-5/qvector.html
     */
    QVector<GpioPin *> m_gpioContainer{};
    Serializer *m_serializer = nullptr;

};
