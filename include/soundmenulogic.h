#ifndef SOUNDMENULOGIC_H
#define SOUNDMENULOGIC_H

#include <QObject>
#include <QQmlApplicationEngine>

class SoundMenuLogic : public QObject
{
    Q_OBJECT
public:

    enum class PIN_STATE{
        e_LOW = 0,
        e_HI
    };

public:
    SoundMenuLogic(QObject *parent = nullptr);

signals:
    void setSliderValue(unsigned char a_value);

public slots:
    void sliderValueChenge(unsigned char a_value);
    void muteButtonPressed(bool a_checked);

private:
  void setMutePin(PIN_STATE a_state);
  void sendToSpi(unsigned char a_value);

private:
    unsigned char m_sliderValue{};
    bool muteButtonChecked{};

    // WiringPI GPIO on Raspberry Pi.
    static constexpr int MUTE_PIN = 29;
    static constexpr int SPI_SPEED = 2000000;
    static constexpr int SPI_CHANNEL = 0;
};

#endif // SOUNDMENULOGIC_H
