#ifndef UTILS_H
#define UTILS_H

#include <QObject>

class Utils : public QObject
{
    Q_OBJECT
public:
    Utils(QObject *parent = nullptr);

    Q_INVOKABLE void shutdownSystem();
signals:

public slots:
};

#endif // UTILS_H
