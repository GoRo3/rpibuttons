#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <QObject>
#include <QVector>
#include <QDebug>

#include "include/gpiopin.h"

static constexpr char DEFAULT_FILE[] = "rpi.save";
static constexpr char DEFAULT_PATH[] = "Rpi3_Buttons";

class Serializer {
public:
    Serializer();

    explicit Serializer(const QVector<GpioPin*> &a_value);

    void setDefaultPath(const QString &a_path);

    bool saveConteiner(const QVector<GpioPin*> &a_object);
    const QVector<GpioPin*>loadContainer();

private:
    QString m_filename{};
    QString m_path{};
    QVector<GpioPin*> m_value{};
};

#endif // SERIALIZER_H
