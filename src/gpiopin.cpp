#include <QDataStream>
#include <QDebug>

#include "wiringPi.h"

#include "include/gpiopin.h"

GpioPin::GpioPin()
{}

GpioPin::GpioPin(int a_pin, GpioPin::Direction a_pinDirection) :
    m_pinNumber(a_pin),
    m_pinDirection(a_pinDirection)
{
    pinMode(m_pinNumber,static_cast<int>(a_pinDirection));
}

int GpioPin::pinNumber() const
{
    return m_pinNumber;
}

void GpioPin::setPinNumber(int pinNumber)
{
    pinMode(pinNumber,static_cast<int>(m_pinDirection));
    m_pinNumber = pinNumber;
}

GpioPin::State GpioPin::pinState() const
{
    m_pinState = static_cast<GpioPin::State>(digitalRead(m_pinNumber));
    return m_pinState;
}

void GpioPin::setPinState(State pinState)
{
    digitalWrite(m_pinNumber,static_cast<int>(pinState));
    m_pinState = pinState;
}

GpioPin::Direction GpioPin::pinDirection() const
{
    m_pinDirection = static_cast<GpioPin::Direction>(digitalRead(m_pinNumber));
    return m_pinDirection;
}

void GpioPin::setPinDirection(Direction pinDirection)
{
    pinMode(m_pinNumber,static_cast<int>(pinDirection));
    m_pinDirection = pinDirection;
}

void GpioPin::toglePin()
{
    if (pinState() == GpioPin::State::eLow) {
       setPinState(GpioPin::State::eHigh);
       m_pinState = GpioPin::State::eHigh;
    } else {
        setPinState(GpioPin::State::eLow);
        m_pinState = GpioPin::State::eLow;
    }
}

QString GpioPin::pinName() const
{
    return m_pinName;
}

void GpioPin::setPinName(const QString &pinName)
{
    m_pinName = pinName;
}

QDataStream &operator<<(QDataStream &out, const GpioPin &pin) {
    out << pin.pinNumber() << pin.pinName();
    return out;
}

QDataStream &operator>>(QDataStream &in, GpioPin &pin) {
 int number;
 QString name;

 in >>  number >> name;

 pin = GpioPin(number);
 pin.setPinName(name);

 return in;
}
