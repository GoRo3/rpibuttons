#include <QAbstractListModel>
#include <QDebug>
#include <QModelIndex>

#include "include/gpiopinmodel.h"

GpioPinModel::GpioPinModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

int GpioPinModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    if (parent.isValid()) {
        return 0;
    }
    return m_gpioContainer.size();
}
QVariant GpioPinModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid()) {
        return {};
    }
    if (index.row() < 0 || index.row() >= m_gpioContainer.size()) {
        return {};
    }

    auto device = m_gpioContainer.at(index.row());

    switch (role) {

    case GpioPinModel::roleNames::PIN_E : {
        return device->pinNumber();
    }

    case GpioPinModel::roleNames::STATE_E : {
        return static_cast<int>(device->pinState());
    }

    case GpioPinModel::roleNames::DIRECTION_E : {
        return static_cast<int>(device->pinDirection());
    }
    case GpioPinModel::roleNames::NAME_E : {
        return device->pinName();
    }
    default:
        return {};
    }
}

QHash<int, QByteArray> GpioPinModel::roleNames() const
{
    QHash<int,QByteArray> roles {
        {GpioPinModel::roleNames::DIRECTION_E, "direction"},
        {GpioPinModel::roleNames::PIN_E,"pin"},
        {GpioPinModel::roleNames::STATE_E,"state"},
        {GpioPinModel::roleNames::NAME_E,"name"}};
    return roles;
}

Qt::ItemFlags GpioPinModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return static_cast<Qt::ItemFlags>(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable);
}

bool GpioPinModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid())
    {
        return false;
    }

    auto pin = m_gpioContainer.at(index.row());

    if( role == GpioPinModel::roleNames::NAME_E) {
        pin->setPinName(value.toString());
    }

    emit dataChanged(index,index,{Qt::EditRole, Qt::DisplayRole});
    return true;
}

void GpioPinModel::addItem(GpioPin *a_gpioPin)
{
    m_gpioContainer.push_back(a_gpioPin);
}

void GpioPinModel::toglePin(int index)
{
    if (index <= m_gpioContainer.size()) {
        m_gpioContainer[index]->toglePin();
        emit dataChanged(createIndex(index,0), createIndex(index,0), {});
    }
}

bool GpioPinModel::saveModel()
{
    return m_serializer->saveConteiner(m_gpioContainer);
}

bool GpioPinModel::loadModel()
{
    const auto conteiner = m_serializer->loadContainer();
    if(!conteiner.isEmpty()) {
        setGpioContainer(conteiner);
        return true;
    }
    return false;
}
void GpioPinModel::setGpioContainer(const QVector<GpioPin *> gpioContainer)
{
    beginResetModel();

    m_gpioContainer = gpioContainer;

    endResetModel();
}

void GpioPinModel::setSerializer(Serializer *serializer)
{
    m_serializer = serializer;
}


