#include <QObject>
#include <QVector>
#include <QFile>
#include <QDebug>
#include <QDataStream>
#include <QDir>

#include "include/serializer.h"

Serializer::Serializer():
    m_filename(DEFAULT_FILE),
    m_path(DEFAULT_PATH)
{
    QString home = QDir::home().path();
    home += "/" + m_path;
    setDefaultPath(home);
}

Serializer::Serializer(const QVector<GpioPin *> &a_value) :
    m_filename(DEFAULT_FILE),
    m_path(DEFAULT_PATH),
    m_value(a_value)
{
    QString home = QDir::home().path();
    home += "/" + m_path;
    setDefaultPath(home);
}

void Serializer::setDefaultPath(const QString &a_path) {
    m_path = a_path;
}

bool Serializer::saveConteiner(const QVector<GpioPin *> &a_object) {
    {
        QDir dir(m_path) ;
        if (!dir.exists()) {
            dir.mkpath(m_path);
        }

        QFile saveFile(dir.filePath(m_filename));

        if(!saveFile.open(QIODevice::WriteOnly)) {
            qDebug() << "Unable to open file " << saveFile.fileName();
            return false;
        }

        QDataStream out(&saveFile);
        out.setVersion(QDataStream::Qt_5_12);

        for(const auto item: a_object) {
            out << *item;
        }

        saveFile.flush();
        saveFile.close();

        return true;
    }
}

const QVector<GpioPin *> Serializer::loadContainer()
{
    QVector<GpioPin *> tempVector;

    QDir dir(m_path);
    QFile loadFile(dir.filePath(m_filename));

    if(!loadFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open file " << loadFile.fileName();
        return {};
    }

    QDataStream in(&loadFile);
    in.setVersion(QDataStream::Qt_5_12);

    while(!in.atEnd()) {
        GpioPin *temp = new GpioPin{};
        in >> *temp;
        tempVector.push_back(temp);
    }

    loadFile.close();

    return tempVector;
}
