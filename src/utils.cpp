#include <QProcess>

#include "include/utils.h"

Utils::Utils(QObject *parent) : QObject(parent)
{}

void Utils::shutdownSystem()
{
    QProcess shutdown;
    shutdown.startDetached("sudo shutdown -P now");
}
