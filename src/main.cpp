#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QThread>
#include <QtDebug>
#include <QQmlContext>
#include <QQuickStyle>
#include <QCursor>

#include <wiringPi.h>

#include "include/gpiopinmodel.h"
#include "include/serializer.h"
#include "include/utils.h"
#include "include/soundmenulogic.h"

/*
 * Funkcja Main.

        Funkcja "main" czyli główny punkt początkowy programu.
        W przypadku programów zbudowanych w oparciu o Qt, w tej funkcji
        tworzymy najważniejsze elementy odpowiedzialne zarówno za logikę oraz
        prezentację aplikacji.

        Do warstwy logik inależą takie elementy jak "wiringPiSetup()", "fillModel()",
        oraz obiekt klasy GuiPinModel. Reszta obiektów odpowiada za warstwę prezentacji
        oraz "silnik" graficznego interfejsu.

        Program korzysta z wbudowanej w Raspberiana biblioteki WiringPi. Aby wszystko działało
        poprawnie program powinien być uruchomiony z uprawnieniami root'a.

        Ponieważ korzystamy z biblioteki WiringPi należy ustawić piny z numeracją zgodną z ową biblioteką.
        Poniżej przedstawiona jest tabelka, dzięki której można łatwo zmapować piny przy podłączaniu
        zewnętrznych elementów.

+-----+-----+---------+------+---+-Pi 3------+---+------+---------+-----+-----+
 | BCM | wPi |   Name  | Mode | V | Physical | V | Mode | Name    | wPi | BCM |
 +-----+-----+---------+------+---+----++----+---+------+---------+-----+-----+
 |     |     |    3.3v |      |   |  1 || 2  |   |      | 5v      |     |     |
 |   2 |   8 |   SDA.1 |  OUT | 0 |  3 || 4  |   |      | 5v      |     |     |
 |   3 |   9 |   SCL.1 |  OUT | 1 |  5 || 6  |   |      | 0v      |     |     |
 |   4 |   7 | GPIO. 7 |  OUT | 0 |  7 || 8  | 0 | OUT  | TxD     | 15  | 14  |
 |     |     |      0v |      |   |  9 || 10 | 0 | OUT  | RxD     | 16  | 15  |
 |  17 |   0 | GPIO. 0 |  OUT | 0 | 11 || 12 | 0 | OUT  | GPIO. 1 | 1   | 18  |
 |  27 |   2 | GPIO. 2 |  OUT | 0 | 13 || 14 |   |      | 0v      |     |     |
 |  22 |   3 | GPIO. 3 |  OUT | 0 | 15 || 16 | 0 | OUT  | GPIO. 4 | 4   | 23  |
 |     |     |    3.3v |      |   | 17 || 18 | 0 | OUT  | GPIO. 5 | 5   | 24  |
 |  10 |  12 |    MOSI |  OUT | 0 | 19 || 20 |   |      | 0v      |     |     |
 |   9 |  13 |    MISO |  OUT | 0 | 21 || 22 | 0 | OUT  | GPIO. 6 | 6   | 25  |
 |  11 |  14 |    SCLK |  OUT | 0 | 23 || 24 | 0 | OUT  | CE0     | 10  | 8   |
 |     |     |      0v |      |   | 25 || 26 | 0 | OUT  | CE1     | 11  | 7   |
 |   0 |  30 |   SDA.0 |   IN | 1 | 27 || 28 | 1 | IN   | SCL.0   | 31  | 1   |
 |   5 |  21 | GPIO.21 |   IN | 1 | 29 || 30 |   |      | 0v      |     |     |
 |   6 |  22 | GPIO.22 |   IN | 1 | 31 || 32 | 0 | IN   | GPIO.26 | 26  | 12  |
 |  13 |  23 | GPIO.23 |   IN | 0 | 33 || 34 |   |      | 0v      |     |     |
 |  19 |  24 | GPIO.24 |   IN | 0 | 35 || 36 | 0 | IN   | GPIO.27 | 27  | 16  |
 |  26 |  25 | GPIO.25 |   IN | 0 | 37 || 38 | 0 | IN   | GPIO.28 | 28  | 20  |
 |     |     |      0v |      |   | 39 || 40 | 0 | IN   | GPIO.29 | 29  | 21  |
 +-----+-----+---------+------+---+----++----+---+------+---------+-----+-----+
 | BCM | wPi |   Name  | Mode | V | Physical | V | Mode | Name    | wPi | BCM |
 +-----+-----+---------+------+---+-Pi 3-----+---+------+---------+-----+-----+

        UWAGA - Aby aplikacjia była łatwo przenoszalna pomiędzy maszyną developerską
        a produkcyjną, jest ona budowana w oparciu o statyczne linkowanie bibliotek Qt.
        W przypadku gdyby użytkownik chciał kompilować ten program na raspberry pi, należy
        w pliku Rpi3_Buttons.pro a pozycji "CONFIG += c++11 static link_pkgconfig" usunąć
        pole "static". Inaczej mogą pojawić się błędy podczas kompilacji.
*/

/*
 * Jest to sposób definiowania stałych w CPP, które będą wstawione w kod podczas
 * kompilacji.
 *
 * Można powiedzieć że jest to odpowienik #define, jednak jest to lepsza metoda,
 * ponieważ kompilator sprawdzi czy np. typy się sgadzają.
 */
static constexpr int NUMBER_OF_GPIO = 15;

/*
 * Deklaracjia funkcji pomicniczej  fillmodel().
 */
void fillModel(GpioPinModel *a_model);
QVector<GpioPin *> createModel();

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    qputenv("QT_QPA_FONTDIR","/usr/share/fonts/truetype/dejavu");
    qputenv("QT_QUICK_CONTROLS_STYLE","material");

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setOverrideCursor(QCursor(Qt::BlankCursor));
/*
 * uruchomienie konfiguracji sterownika pinów.
 */
    wiringPiSetup();
/*
 * Instancja klasy GpioPinModel odpowiadająca za komunikację z warstwą aplikacji
 * oraz warstwą logiki.
 */
    GpioPinModel *model = new GpioPinModel;
    Serializer *serializer = new Serializer;
    Utils *utils = new Utils();
    SoundMenuLogic soundLogic(&app);

    model->setGpioContainer(createModel());
    model->setSerializer(serializer);
/*
 * Poniżej tworzymy instancję klasy QQmlApplicationEngine, która odpowiada min.
 * za łączenie warstwy graficznej aplikacji z warstwą ligiczną napisaną w cpp.
 *
 * Odpowłując się do odpowiednich metod tej klasy możemy przekaząc do wartswy prezentacji
 * instację kllasy, sygnały lub zmienne stworzony w warstwie logicznej.
 */
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

/*
 * W tym miejscu przekazujemy wskaźnik na klasę GpioPinModel do warstwy aplikacji
 * aby móc się komunikować z elementami znajdującymi się w modelu.
 */
    engine.rootContext()->setContextProperty("utils", utils);
    engine.rootContext()->setContextProperty("gpioModel",model);
    engine.rootContext()->setContextProperty("soundLogic", &soundLogic);
    engine.load(url);

//Główna pętla aplikacji.
    return app.exec();
}

/*
 * Prosta funkcja wypełniająca model instancjami klasy GpioPIn, aby móc się
 * z komunikować z nimi przy pomocy interfejsu dotykowego.
 */
void fillModel(GpioPinModel *a_model)
{
    for(int i = 0; i < NUMBER_OF_GPIO; ++i ) {
        GpioPin* pin = new GpioPin(i);
        a_model->addItem(pin);
    }
};

QVector<GpioPin *> createModel() {
    QVector<GpioPin *> tempModel = {
        new GpioPin(0),
        new GpioPin(1),
        new GpioPin(2),
        new GpioPin(3),
        new GpioPin(4),
        new GpioPin(5),
        new GpioPin(6),
        new GpioPin(7),
        new GpioPin(8),
        new GpioPin(9),
        new GpioPin(10),
        new GpioPin(11),
        new GpioPin(15),
        new GpioPin(16),
        new GpioPin(17),
    };
    return tempModel;
}
