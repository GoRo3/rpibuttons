#include <QObject>
#include <QtDebug>

#include <wiringPi.h>
#include <wiringPiSPI.h>

#include "include/soundmenulogic.h"

SoundMenuLogic::SoundMenuLogic(QObject *parent) :
    QObject(parent)
{
    pinMode(MUTE_PIN, 1);
    digitalWrite(MUTE_PIN, 1);
    wiringPiSPISetup(SPI_CHANNEL, SPI_SPEED);
}

void SoundMenuLogic::sliderValueChenge(unsigned char a_value)
{
    if(muteButtonChecked) {
        return;
    }

    m_sliderValue = a_value;
    sendToSpi(m_sliderValue);
}

void SoundMenuLogic::muteButtonPressed(bool a_checked)
{
    muteButtonChecked = a_checked;

    if(a_checked) {
        setMutePin(PIN_STATE::e_HI);
        sendToSpi(0);
        emit setSliderValue(0);
    } else {
        setMutePin(PIN_STATE::e_LOW);
        sendToSpi(m_sliderValue);
        emit setSliderValue(m_sliderValue);
    }
}

void SoundMenuLogic::setMutePin(SoundMenuLogic::PIN_STATE a_state)
{
    if (a_state == SoundMenuLogic::PIN_STATE::e_HI) {
        digitalWrite(MUTE_PIN, 0);
    } else if (a_state == SoundMenuLogic::PIN_STATE::e_LOW) {
        digitalWrite(MUTE_PIN, 1);
    }
}

void SoundMenuLogic::sendToSpi(unsigned char a_value)
{
    wiringPiSPIDataRW(SPI_CHANNEL, &a_value,1);
    wiringPiSPIDataRW(SPI_CHANNEL, &a_value,1);
}
