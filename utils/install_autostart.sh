#!/bin/bash

sudo chmod 644 buttons.service
sudo cp buttons.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable buttons.service
exit 0

