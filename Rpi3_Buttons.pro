QT += quick qml quickcontrols2 core
CONFIG += c++1z
#CONFIG += c++1z static link_pkgconfig

static {
    QT += svg
    QTPLUGIN += qtvirtualkeyboardplugin
}

LIBS += -L/usr/local/lib/libwiringPi.so -lwiringPi

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        src/gpiopin.cpp \
        src/gpiopinmodel.cpp \
        src/main.cpp \
        src/serializer.cpp \
        src/soundmenulogic.cpp \
        src/utils.cpp

HEADERS += \
    include/gpiopin.h \
    include/gpiopinmodel.h \
    include/serializer.h \
    include/soundmenulogic.h \
    include/utils.h

RESOURCES += qml/qml.qrc \
    resources/icons/icons.qrc

#Release:DESTDIR = {$$PWD}/../release
#Release:OBJECTS_DIR = {$$PWD}/../release/.obj
#Release:MOC_DIR = {$$PWD}/../release/.moc
#Release:RCC_DIR = {$$PWD}/../release/.rcc
#Release:UI_DIR = {$$PWD}/../release/.ui

#Debug:DESTDIR = {$$PWD}/../debug
#Debug:OBJECTS_DIR = {$$PWD}/../debug/.obj
#Debug:MOC_DIR = {$$PWD}/../debug/.moc
#Debug:RCC_DIR = {$$PWD}/../debug/.rcc
#Debug:UI_DIR = {$$PWD}/../debug/.ui

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
#qnx: target.path = /usr/bin/
#else: unix:!android: target.path = /usr/bin/
qnx: target.path = /home/pi/$${TARGET}
else: unix:!android: target.path = /home/pi/$${TARGET}
!isEmpty(target.path): INSTALLS += target

